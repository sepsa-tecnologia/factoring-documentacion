-- admin
insert into info.persona(id, id_tipo_persona, ruc, dv_ruc, estado) values(100003, 1, '0', '0', 'A');

-- admin: usuario (password: 123)
insert into trans.usuario(id, id_persona, usuario, contrasena, estado) values(100003, 100003, 'admin', '$2a$10$h5uYE1GhRcZi6FwM.34rCeVs9LCf8/j9JQR42dl/EzjOiZXoZj5Py', 'A');
insert into factoring.usuario_perfil(id_usuario, id_perfil) values(100003, 4);