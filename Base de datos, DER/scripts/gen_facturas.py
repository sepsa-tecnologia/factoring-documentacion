from datetime import datetime
from datetime import date
from datetime import timedelta
import random

EXPORT_CSV = 'csv'
EXPORT_SQL = 'sql'


def gen_number():
    n = random.randint(4, 7)
    s = str(n)
    for x in range(n):
        s = s + str(random.randint(0, 9))
    return s


fecha_inicio = date.today() + timedelta(days=1)
comprador_id = 200000
comprador_razon_social = "EL COMPRADOR II S.A."
comprador_ruc = "123456-7"
proveedor_id = 200001
proveedor_razon_social = "EL PROVEEDOR II S.A."
proveedor_ruc = "654321-7"
moneda_id = 1
export = EXPORT_SQL

for i in range(100):
    nro_factura = '001-001-' + str(i + 1).zfill(7)
    fecha_emision = fecha_inicio
    fecha_pago = fecha_inicio + timedelta(days=i + 20)
    monto_total_iva5 = "0"
    monto_total_iva10 = "0"
    monto_total_exentas = gen_number()
    monto_total_factura = monto_total_exentas
    monto_nota_credito = "0"
    monto_retencion = "0"
    monto_otros = "0"
    monto_total_imponible = monto_total_exentas
    conformar = "S" if random.choice([True, False]) == True else "N"
    if export == EXPORT_CSV:
        print('{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}'.format(
            nro_factura,
            comprador_razon_social,
            comprador_ruc,
            datetime.strftime(fecha_emision, '%d-%m-%Y'),
            datetime.strftime(fecha_pago, '%d-%m-%Y'),
            monto_total_iva5,
            monto_total_iva10,
            monto_total_exentas,
            monto_total_factura,
            monto_nota_credito,
            monto_retencion,
            monto_otros,
            monto_total_imponible,
            conformar
        ))
    elif export == EXPORT_SQL:
        print("insert into factoring.factura (id_lote, id_moneda, id_persona_origen, id_persona_destino, id_documento, id_tipo_documento, id_estado, nro_factura, nro_timbrado, vto_timbrado, razon_social_comprador, ruc_comprador, direccion_comprador, telefono_comprador, razon_social_proveedor, ruc_proveedor, direccion_proveedor, telefono_proveedor, fecha, fecha_pago, dias_credito, monto_iva_5, monto_imponible_5, monto_total_5, monto_iva_10, monto_imponible_10, monto_total_10, monto_total_exento, monto_iva_total, monto_imponible_total, monto_total_factura, hash) values (NULL, {0}, {1}, {2}, NULL, NULL, 1, '{3}', '123123123123', 'DICIEMBRE/2050', '{4}', '{5}', NULL, NULL, '{6}', '{7}', NULL, NULL, '{8}', '{9}', {10}, 0, 0, 0, 0, 0, 0, {11}, 0, {11}, {11}, NULL);".format(
            moneda_id,
            proveedor_id,
            comprador_id,
            nro_factura,
            comprador_razon_social,
            comprador_ruc,
            proveedor_razon_social,
            proveedor_ruc,
            datetime.strftime(fecha_emision, '%Y-%m-%d'),
            datetime.strftime(fecha_pago, '%Y-%m-%d'),
            i + 20,
            monto_total_exentas
        ))
    else:
        pass
